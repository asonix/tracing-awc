# tracing-awc
Instrument the AWC client with tracing, and optionally opentelemetry for propagating OpenTelemetry spans


## Usage
```toml
actix-rt = "2.6"
awc = "3.0.0"
tracing = "0.1"
tracing-futures = "0.2"
tracing-subscriber = { version = "0.2", features = ["fmt"] }
tracing-awc = { version = "0.1.0", features = ["opentelemetry_0_17"]
```
```rust
use awc::Client;
use std::error::Error;
use tracing_awc::{root_span, Tracing};
use tracing_futures::Instrument;

async fn request(url: &str) -> Result<(), Box<dyn Error>> {
    Client::builder()
        .wrap(Tracing)
        .finish()
        .get(url)
        .send()
        .await?;

    Ok(())
}

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn Error>> {
    init_opentelemetry("http://localhost:4317")?;

    request("http://localhost:9000")
        .instrument(root_span())
        .await
}

// See examples/client.rs for an implementation of init_opentelemetry
```


## Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the AGPLv3.


## License

Copyright © 2021 Riley Trautman

tracing-awc-opentelemetry is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

tracing-awc-opentelemetry is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of tracing-awc-opentelemetry.

You should have received a copy of the GNU General Public License along with tracing-awc-opentelemetry. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
