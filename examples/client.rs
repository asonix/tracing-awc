use awc::Client;
use opentelemetry_otlp::WithExportConfig;
use std::error::Error;
use tracing::instrument::Instrument;
use tracing_awc::{root_span, Tracing};
use tracing_subscriber::{fmt::format::FmtSpan, layer::SubscriberExt, EnvFilter, Registry};

use opentelemetry_0_21_pkg as opentelemetry;
use tracing_opentelemetry_0_22_pkg as tracing_opentelemetry;

use opentelemetry::KeyValue;
use opentelemetry_sdk::{propagation::TraceContextPropagator, Resource};

async fn request(url: &str) -> Result<(), Box<dyn Error>> {
    Client::builder()
        .wrap(Tracing)
        .finish()
        .get(url)
        .send()
        .await?;

    Ok(())
}

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn Error>> {
    init_opentelemetry("http://localhost:4317")?;

    request("http://localhost:9000")
        .instrument(root_span())
        .await
}

fn init_opentelemetry(url: &str) -> Result<(), Box<dyn Error>> {
    opentelemetry::global::set_text_map_propagator(TraceContextPropagator::new());

    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));

    let format_layer = tracing_subscriber::fmt::layer()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .pretty();

    let subscriber = Registry::default().with(env_filter).with(format_layer);

    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_trace_config(
            opentelemetry_sdk::trace::config().with_resource(Resource::new(vec![KeyValue::new(
                "service.name",
                "client-demo",
            )])),
        )
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_endpoint(url),
        )
        .install_batch(opentelemetry_sdk::runtime::Tokio)?;

    let otel_layer = tracing_opentelemetry::layer().with_tracer(tracer);

    let subscriber = subscriber.with(otel_layer);

    tracing::subscriber::set_global_default(subscriber)?;

    Ok(())
}
