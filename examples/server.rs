// This files exists as a companion to the client example, and does not demostrate
// tracing-awc-opentelemetry itself

use actix_web::{web, App, HttpServer, Responder};
use opentelemetry_otlp::WithExportConfig;
use std::error::Error;
use tracing_actix_web::TracingLogger;
use tracing_subscriber::{fmt::format::FmtSpan, layer::SubscriberExt, EnvFilter, Registry};

use opentelemetry_0_21_pkg as opentelemetry;
use tracing_opentelemetry_0_22_pkg as tracing_opentelemetry;

use opentelemetry::KeyValue;
use opentelemetry_sdk::{propagation::TraceContextPropagator, Resource};

#[tracing::instrument(name = "Index")]
async fn index() -> impl Responder {
    tracing::info!("Hello!");
    "Hello!"
}

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn Error>> {
    init_opentelemetry("http://localhost:4317")?;

    HttpServer::new(|| {
        App::new()
            .wrap(TracingLogger::default())
            .route("/", web::get().to(index))
    })
    .bind("127.0.0.1:9000")?
    .run()
    .await?;

    Ok(())
}

fn init_opentelemetry(url: &str) -> Result<(), Box<dyn Error>> {
    opentelemetry::global::set_text_map_propagator(TraceContextPropagator::new());

    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));

    let format_layer = tracing_subscriber::fmt::layer()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .pretty();

    let subscriber = Registry::default().with(env_filter).with(format_layer);

    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_trace_config(
            opentelemetry_sdk::trace::config().with_resource(Resource::new(vec![KeyValue::new(
                "service.name",
                "server-demo",
            )])),
        )
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_endpoint(url),
        )
        .install_batch(opentelemetry_sdk::runtime::Tokio)?;

    let otel_layer = tracing_opentelemetry::layer().with_tracer(tracer);

    let subscriber = subscriber.with(otel_layer);

    tracing::subscriber::set_global_default(subscriber)?;

    Ok(())
}
